#!/usr/bin/env sh

set -eu

sed -i "s/\/raw\/.*\//\/raw\/v${1}\//g" main.gitlab-ci.yml
sed -i "s/\/raw\/.*\//\/raw\/v${1}\//g" README.md
