# gitlab-ci-templates

This repository contains GitLab CI/CD templates that can be used in other projects.

## main.gitlab-ci.yml

This GitLab CI script contains templates for workflow rules, stages and jobs. To include it, use the following text in your `.gitlab-ci.yml` file:

```yaml
include:
  - remote: https://gitlab.com/omnitoolkit/gitlab-ci-templates/-/raw/v3.3.0/main.gitlab-ci.yml
```

The templates are not active by default and need to be referenced in order to be used. To do this, use the following text in your `.gitlab-ci.yml` file and, optionally, add custom config to override values from the template.

```yaml
# for workflow rules and stages
workflow: !reference [.name_of_desired_template]

# for jobs
my_job:
  extends: .name_of_desired_template
```

### .omnitoolkit_ci__workflow_rules_git_version_tag

These workflow rules check if the current reference is a git version tag, e.g. 'v1.0.0', and replace the variable `CI_COMMIT_REF_SLUG` with it's content. This is to prevent docker tags for git version tags from looking like 'v1-0-0' while still using the [syntax](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

### .omnitoolkit_ci__stages

This template, in addition to the default stages, provides a stage for checks at the beginning and a stage for releases at the end.

### .omnitoolkit_ci__job_docker_dind_build_and_push

This job builds a docker image via Docker-in-Docker (DinD) with the current reference (branch or tag) as docker tag and pushes it to the GitLab container registry. Build cache will be used if available. To customize, change the `BUILD_*` variables in your job.

### .omnitoolkit_ci__job_deploy_docker_compose_v1_via_ssh:

This job copies and executes a docker compose v1 script and optionally copies custom files and folders to a target server via ssh. The `DEPLOY_SSH_*` variables must be changed, ideally via GitLab CI variables. Connections via ssh from the GitLab runner's IP address need to be allowed in the firewall of the target server. To customize, create a `before_script` as shown in the examples below:

```yaml
before_script:
  # Create '.env' file and replace variables, ideally via GitLab CI variables.
  - cp .env.example .env
  - sed -i "s/^MY_VARIABLE=.*/MY_VARIABLE=${MY_VARIABLE}/g" .env

  # Create 'docker-compose.override.yml' to customize docker compose script.
  - cp docker-compose.ci-deploy.yml docker-compose.override.yml

  # Create custom files and folders which will be copied to target host.
  - mkdir -p ./deploy/bin/
  - cp ./bin/*.sh ./deploy/bin/

  # Create custom deploy script, default script will be added below.
  - mkdir -p ./deploy/
  - echo -e "#!/usr/bin/env sh\n
      set -eu\n
      /my/cool/script.sh" > ./deploy/deploy.sh
```

### .omnitoolkit_ci__job_release_version_with_releasebot

This job releases a new version with releasebot according to conventional commits and semantic versioning. Check the [repository](https://gitlab.com/omnitoolkit/releasebot) of releasebot for more information. The variable `RELEASE_BRANCHES` contains one or more space-separated branches that releases will be generated for, and `RELEASE_DRY_RUN` can be set to `"true"` (the quotes are important) to only show the new release changelog.
