## [3.3.0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v3.2.1...v3.3.0) (2024-02-23)


### Features

* add job for deploying ansible collection ([6d297bb](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/6d297bb8ba6299f1012dfcf3ed6f24d4d57fe143))

## [3.2.1](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v3.2.0...v3.2.1) (2023-07-26)


### Bug Fixes

* install python packages required by ansible modules ([329312f](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/329312f54bfcb81398a09b6894e6f498794e575d))
* use absolute path for requirements file ([722562d](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/722562d642d947ebfb9eb9860694be0d22d952ba))

## [3.2.0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v3.1.1...v3.2.0) (2023-07-23)


### Features

* add job for deploying ansible playbooks ([0ecded1](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/0ecded1d81293659727347dd6a302ad4f46fa266))

## [3.1.1](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v3.1.0...v3.1.1) (2023-02-23)


### Documentation

* remove quote from remote url ([54f3d86](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/54f3d86832a488fc57ff11fc47aa56f8cd2609d4))

## [3.1.0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v3.0.0...v3.1.0) (2023-02-23)


### Features

* use correct version tag for remote url ([74a4d72](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/74a4d729f6e3a743200547cc73a327138ff08a3c))

## [3.0.0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v2.0.0...v3.0.0) (2023-02-23)


### ⚠ BREAKING CHANGES

* rename template file to main.gitlab-ci.yml
* rename repository to gitlab-ci-templates

### Features

* rename repository to gitlab-ci-templates ([9064bba](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/9064bba98b20334748f5677beb57715132d90502))
* rename template file to main.gitlab-ci.yml ([3ea2692](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/3ea2692f3ea98f02d5d9858198b33d0866c4a625))


### Continuous Integration

* autoupdate version number in documentation ([9ac0659](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/9ac0659817a7cbd03699851a925d363ec7f7a921))


### Documentation

* add information to readme ([67678a4](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/67678a4a2e85b77510784a7d41fb33908140cd88))
* add template descriptions to readme ([87a2180](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/87a21807390af4b7b286788ed6267da3e3fabad5))
* fix markdown syntax ([05ae18d](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/05ae18dd662da6eb8fd2659e2b67f5439bbb4329))
* fix readme title ([9e7d5ae](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/9e7d5ae9a27cfbb0e5149c4698e9917a63e53d88))
* use correct markdown code syntax ([f96a022](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/f96a022d39aa81144ec107479d02144d06a51983))

## [2.0.0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/compare/v1.0.0...v2.0.0) (2022-11-26)


### ⚠ BREAKING CHANGES

* update releasebot image to v2.0.0

### Features

* update releasebot image to v2.0.0 ([b109bc0](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/b109bc0025aa27a90d56166913d418029e74c458))


### Continuous Integration

* remove test message since there is now a real job ([506a1ed](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/506a1ed2248a5be92cc01f9555630118eecf1a93))
* set release job to run manually ([c3f18e8](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/c3f18e80babb249f6fea4db7e86b9d5f09b326c3))

## 1.0.0 (2022-11-08)


### Features

* add gitlab ci templates ([9ffd7f1](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/9ffd7f1a894a293efe3fd2779b258bd04122f566))
* add job template for release version ([87d1b9e](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/87d1b9ef8b892af373474e5c88f468e25ac04366))
* add release to ci ([39bb95b](https://gitlab.com/omnitoolkit/gitlab-ci-templates/commit/39bb95b46aa527ae81e44752db6201a3720b3d62))
